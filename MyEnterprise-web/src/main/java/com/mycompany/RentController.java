/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.model.Book;
import com.mycompany.model.Rent;
import com.mycompany.model.User;
import java.util.Date;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Msi
 */
@ManagedBean(name = "rentController")
@ApplicationScoped
public class RentController {
    private User user = new User();
    private Book book = new Book();
    private Date rentDate;
    private Integer id_user;
    private Integer id_book;

    public void setUser(User user) {
        this.user = user;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public void setRentDate(Date rentDate) {
        this.rentDate = rentDate;
    }

    public User getUser() {
        return user;
    }

    public Book getBook() {
        return book;
    }

    public Date getRentDate() {
        return rentDate;
    }
    
    @EJB
    private RentBeanIfc bean;
    
    public void save() {
        Rent rent = new Rent();
        rent.setBook(this.getBook());
        rent.setUser(this.getUser());
        rent.setRentDate(new Date());
        bean.rentBook(rent);
    }
    
    

    /**
     * Creates a new instance of RentController
     */
    public RentController() {
    }
    
}
