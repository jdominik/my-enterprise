
import com.mycompany.ConverterBean;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

// You must annotate the converter as a managed bean, if you want to inject
// anything into it, like your persistence unit for example.

@ManagedBean(name = "categoryConverterBean")
@FacesConverter(value = "categoryConverter")
@ApplicationScoped
public class CategoryConverter {

    
    private Converter bean;

    
    public Object getAsObject(FacesContext ctx, UIComponent component,
            String value) {
        // This will return the actual object representation
        // of your Category using the value (in your case 52) 
        // returned from the client side
        return bean.getAsObject(ctx, component, value);
    }

    
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        //This will return view-friendly output for the dropdown menu
        return bean.getAsString(fc, uic, o);
    }
}
