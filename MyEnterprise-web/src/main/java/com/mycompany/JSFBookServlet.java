/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.model.Author;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import com.mycompany.model.Book;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "booksManagerBean")
@ApplicationScoped
public class JSFBookServlet {
    
    private Book book = new Book();
    private String title;
    private Integer ReleaseDate;
    private String authors;
   

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setReleaseDate(Integer ReleaseDate) {
        this.ReleaseDate = ReleaseDate;
    }


    public void setBean(BooksManagerBeanIfc bean) {
        this.bean = bean;
    }

    public Book getBook() {
        return book;
    }
    public String getAuthors() {
        return authors;
    }
   

    public String getTitle() {
        return title;
    }

    public Integer getReleaseDate() {
        return ReleaseDate;
    }


    public BooksManagerBeanIfc getBean() {
        return bean;
    }
    
    @EJB
    private BooksManagerBeanIfc bean;

    public JSFBookServlet() {

    }

    public String getText() {
        //bean.addBook("Tytul ksiazki");
        return "ksiazka";
    }
    
    public List<Book> getList() {
        return bean.getBooks();
    }
    

    //public void addBook(String bookName) {
    //    bean.addBook(bookName);
    //}
    
    public void save() {
        Book book = new Book();
        String[] names = authors.split(",");
        ArrayList<Author> booksAuthors = new ArrayList<>();
        for (String name : names) {
            Author author = new Author();
            author.setName(name);
            ArrayList<Book> arrayList = new ArrayList<>();
            arrayList.add(book);
            author.setBooks(arrayList);
            booksAuthors.add(author);
        }
        book.setReleaseDate(this.getReleaseDate());
        book.setTitle(this.getTitle());
        book.setAuthors(booksAuthors);
        bean.addBook(book);
        
    }
   

    /**
     * Creates a new instance of JSFBookServlet
     */

}
