/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.model.Book;
import com.mycompany.model.User;
import java.util.List;
import javax.ejb.EJB;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "userController")
@ApplicationScoped
public class UserController {
    private User user = new User();
    private String name;

    public void setUser(User user) {
        this.user = user;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public User getUser() {
        return user;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
    private String surname;
    
    @EJB
    private UserBeanIfc bean;
    
    
    public void save() {
        User user = new User();
        user.setName(this.getName());
        user.setSurname(this.getSurname());
        bean.addUser(user);
    }
    
    public List<User> getList() {
        return bean.getUsers();
    }
    
    
    
    /**
     * Creates a new instance of UserControler
     */
    public UserController() {
        
        
        
    }
    
}
