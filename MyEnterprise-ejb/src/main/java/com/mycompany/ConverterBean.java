/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import java.math.BigInteger;
import java.util.Locale;
import javax.ejb.Stateless;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author RENT
 */
@Stateless
public class ConverterBean implements Converter {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @PersistenceContext(unitName = "persistance_unit")
    private EntityManager em;
    @Override
    public Object getAsObject(FacesContext ctx, UIComponent component,
            String value) {
        // This will return the actual object representation
        // of your Category using the value (in your case 52) 
        // returned from the client side
        return em.find(Locale.Category.class, new BigInteger(value));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        //This will return view-friendly output for the dropdown menu
        return ((Locale.Category) o).toString();
    }
}
