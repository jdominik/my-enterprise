/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.model.Author;
import com.mycompany.model.Book;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author RENT
 */
@Stateless
public class BooksManagerBean implements BooksManagerBeanIfc {
    private final ArrayList<String> books;
    


    public BooksManagerBean() {
        this.books = new ArrayList<String>();
    }
    
    @PersistenceContext(unitName = "persistance_unit")
    private EntityManager em;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public List<Book> getBooks() {
        Query query = em.createQuery("FROM Book b");
                return query.getResultList();
        
    }
    @Override
    public void addAuthor(List<Author> author) {

        em.persist(author);
    }
    @Override
    public void addBook(Book book) {
        
        em.persist(book);
    }
}
