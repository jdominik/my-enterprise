/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.model.Book;
import com.mycompany.model.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author RENT
 */
@Stateless
public class UserBean implements UserBeanIfc {
    
    @PersistenceContext(unitName = "persistance_unit")
    private EntityManager em;
    
    @Override
    public void addUser(User user) {
        em.persist(user);
        
    }
    @Override
    public void removeUser (User user) {
        Query query = em.createQuery("DELETE user");
        query.executeUpdate();
    }
    
    public List<User> getUsers() {
        Query query = em.createQuery("FROM User u");
        return query.getResultList();
    }
    
    public void UserBean() {
        
    }
    

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
