/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.model.Rent;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author RENT
 */
@Stateless
public class RentBean implements RentBeanIfc {
    
    @PersistenceContext(unitName = "persistance_unit")
    private EntityManager em;

    @Override
    public void rentBook(Rent rent) {
        em.persist(rent);
    }

    @Override
    public void backBook(Rent rent) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
