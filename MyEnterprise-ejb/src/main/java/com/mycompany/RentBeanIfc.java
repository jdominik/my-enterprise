/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.model.Rent;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author RENT
 */
@Local
@Remote
public interface RentBeanIfc {
    public void rentBook(Rent rent);
    public void backBook(Rent rent);
    
}
