/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.model.Author;
import com.mycompany.model.Book;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author RENT
 */

@Local
@Remote
public interface BooksManagerBeanIfc {

    public List<Book> getBooks();

    public void addBook(Book book);
    public void addAuthor(List<Author> author);
}
